package az.redis_task.redis.lockKeys;

public class LockKeys {
    public static final String CREATE_STUDENT_LOCK = "createStudentLock";
    public static final String GET_STUDENT_LOCK = "getStudentLock";
    public static final String UPDATE_STUDENT_LOCK = "updateStudentLock";
    public static final String DELETE_STUDENT_LOCK = "deleteStudentLock";
}
