package az.redis_task.redis.redis;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class Locking {
        private RedissonClient client;

        public void tryLock(String lockValue) {
            initRedissonClient();
            RLock rLock = client.getLock(lockValue);
            try {
                boolean isLocked = rLock.tryLock(6,
                        10,
                        TimeUnit.MINUTES);

                if (isLocked) {
                    System.err.println("Locked");
                } else {
                    System.err.println("Is not locked");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        public void unlock(String lockValue) {

            client.getLock(lockValue)
                    .unlock();

            shutDownRedissonClient();
        }


        private void initRedissonClient() {
            Config config = new Config();
            config.useSingleServer()
                    .setAddress("127.0.0.1:6379")
                    .setConnectionMinimumIdleSize(24)
                    .setConnectionPoolSize(64);
            client = Redisson.create(config);
        }

        private void shutDownRedissonClient() {
            client.shutdown();
        }

}

