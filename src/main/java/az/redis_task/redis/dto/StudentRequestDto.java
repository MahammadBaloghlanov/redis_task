package az.redis_task.redis.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentRequestDto {

    private String name;
    private String surname;
    private Integer age;


}
