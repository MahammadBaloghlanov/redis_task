package az.redis_task.redis.rest;

import az.redis_task.redis.dto.StudentDto;
import az.redis_task.redis.dto.StudentRequestDto;
import az.redis_task.redis.lockKeys.LockKeys;
import az.redis_task.redis.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public StudentDto create(@RequestBody StudentRequestDto studentRequestDto) {
        return studentService.createStudent(studentRequestDto, LockKeys.CREATE_STUDENT_LOCK);
    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return studentService.getById(id, LockKeys.GET_STUDENT_LOCK);
    }

    @PutMapping("/{id}")
    public StudentDto update(@PathVariable Long id,
                             @RequestBody StudentRequestDto studentRequestDto) {
        return studentService.updateStudent(id, studentRequestDto, LockKeys.UPDATE_STUDENT_LOCK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        studentService.deleteStudent(id,LockKeys.DELETE_STUDENT_LOCK);
    }
}
