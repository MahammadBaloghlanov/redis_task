package az.redis_task.redis.service;

import az.redis_task.redis.dto.StudentDto;
import az.redis_task.redis.dto.StudentRequestDto;
import az.redis_task.redis.entity.StudentEntity;
import az.redis_task.redis.exception.NotFoundException;
import az.redis_task.redis.redis.Locking;
import az.redis_task.redis.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepo studentRepo;
    private final Locking locking;

    @Override
    public StudentDto createStudent(StudentRequestDto studentRequestDto, String lockKey) {
        locking.tryLock(lockKey);
        try {
            StudentEntity student = StudentEntity.builder()
                    .name(studentRequestDto.getName())
                    .surname(studentRequestDto.getSurname())
                    .age(studentRequestDto.getAge())
                    .build();
            final StudentEntity savedStudent = studentRepo.save(student);
            return StudentDto.builder()
                    .id(savedStudent.getId())
                    .name(savedStudent.getName())
                    .surname(savedStudent.getSurname())
                    .age(savedStudent.getAge())
                    .build();
        } finally {
            locking.unlock(lockKey);
        }
    }

    @Override
    public StudentDto getById(Long id, String lockKey) {
        locking.tryLock(lockKey);
        try {
            final StudentEntity student = studentRepo.findById(id)
                    .orElseThrow(() -> new NotFoundException("Student not found"));
            return StudentDto.builder()
                    .id(student.getId())
                    .name(student.getName())
                    .surname(student.getSurname())
                    .age(student.getAge())
                    .build();
        } finally {
            locking.unlock(lockKey);
        }
    }

    @Override
    public StudentDto updateStudent(Long id, StudentRequestDto studentRequestDto, String lockKey) {
        locking.tryLock(lockKey);
        try {
            final StudentEntity student = studentRepo.findById(id)
                    .orElseThrow(() -> new NotFoundException("Student not found"));
            student.setName(studentRequestDto.getName());
            student.setSurname(studentRequestDto.getSurname());
            student.setAge(studentRequestDto.getAge());
            final StudentEntity updatedStudent = studentRepo.save(student);
            return StudentDto.builder()
                    .id(updatedStudent.getId())
                    .name(updatedStudent.getName())
                    .surname(updatedStudent.getSurname())
                    .age(updatedStudent.getAge())
                    .build();
        } finally {
            locking.unlock(lockKey);
        }
    }

    @Override
    public void deleteStudent(Long id, String lockKey) {
        locking.tryLock(lockKey);
        try {
            studentRepo.deleteById(id);
        } finally {
            locking.unlock(lockKey);
        }
    }
}
