package az.redis_task.redis.service;

import az.redis_task.redis.dto.StudentDto;
import az.redis_task.redis.dto.StudentRequestDto;

public interface StudentService {
    StudentDto createStudent(StudentRequestDto studentRequestDto, String lockKey);

    StudentDto getById(Long id, String lockKey);

    StudentDto updateStudent(Long id, StudentRequestDto studentRequestDto, String lockKey);

    void deleteStudent(Long id, String lockKey);
}
