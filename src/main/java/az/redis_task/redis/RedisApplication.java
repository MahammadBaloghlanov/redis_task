package az.redis_task.redis;

import az.redis_task.redis.redis.Locking;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class RedisApplication  {


	public static void main(String[] args) {
		SpringApplication.run(RedisApplication.class, args);
	}


}
